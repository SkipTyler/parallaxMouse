Mouse Parallax Effect

Settings 

    const _parallax = document.querySelector('.js-parallax');
    const _elements = '.js-parallaxItem';
    parallaxMouse(_parallax, _elements);
    
        - _parallax - The element that receives events
        - _elements - Moving elements
    
    
    Events element hav a 'data-parallax-axe' for setting the axis of motion of moving elements
        
        - An empty value is the x and y axis
        - 'x' is the x axis
        - 'y' is the y axis
    
    Moving elements have a 'data-parallax-offset' with a speed multiplier
    
    