'use strict';

const parallaxMouse = (parent, element) => {

	const _elements = parent.querySelectorAll(element);
	const _offset = parent.getBoundingClientRect();
	const _dataAxe = parent.dataset.paralaxAxe;

	parent.addEventListener('mousemove', (ev) => {
		const _mousePos = {
			top: ev.pageY - _offset.top,
			left: ev.pageX - _offset.left
		};
		Array.prototype.forEach.call(_elements, (el) =>{
			
			const _elData = el.dataset.parallaxOffset;
			const _strength = parseInt(_elData);
			let _traX = (_mousePos.left - (_offset.width/3)) * _strength / _offset.width,
				_traY = (_mousePos.top - (_offset.height/3)) * _strength / _offset.height;
			if (_dataAxe === 'x') {
				_traY = '0'
			} else if (_dataAxe === 'y') {
				_traX = '0'
			}
			el.style.transform = "translate(" + _traX + "px, " + _traY + "px"

		});

	});

};

const _parallax = document.querySelector('.js-parallax');
const _elements = '.js-parallaxItem';
parallaxMouse(_parallax, _elements);